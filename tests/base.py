import os
import unittest
from delivery import create_app, db


class BaseTestCase(unittest.TestCase):

    def create_app(self):
        app = create_app()
        app.config.from_object('delivery.config.testing.Configuration')
        app.app_context().push()
        return app


    def setUp(self):
        super(BaseTestCase, self).setUp()
        self.app = self.create_app()
        self.client = self.app.test_client()
        self.fixtures_path = os.path.join(os.path.dirname(__file__), 'fixtures')

        self.header = {'Content-Type': 'application/json',
        'Authorization': 'apikey="%s"' % self.app.signer.sign('adriano')}
        db.drop_all()
        db.create_all()

        self.load_fixtures()


    def load_fixtures(self):
        pass


    def tearDown(self):
        super(BaseTestCase, self).tearDown()
        db.session.remove()
        db.drop_all()