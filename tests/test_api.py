# -*- coding: utf-8 -*-
import json
import datetime

from base import BaseTestCase

from delivery import db
from delivery.models import Route, Map


class APIRESTCase(BaseTestCase):

    def setUp(self):
        super(APIRESTCase, self).setUp()

    def load_fixtures(self):
        maps = Map()
        maps.name = "SP"
        maps.created = datetime.datetime(2016, 1, 1)
        maps.updated = datetime.datetime(2016, 1, 1)
        db.session.add(maps)

        route = Route()
        route.origin = "A"
        route.destination = "B"
        route.distance = "10"
        route.created = datetime.datetime(2016, 1, 1)
        route.updated = datetime.datetime(2016, 1, 1)
        route.map_id = 1
        db.session.add(route)

        route = Route()
        route.origin = "B"
        route.destination = "D"
        route.distance = "15"
        route.created = datetime.datetime(2016, 1, 1)
        route.updated = datetime.datetime(2016, 1, 1)
        route.map_id = 1
        db.session.add(route)

    def test_get_maps(self):
        response = self.client.get('/api/maps', headers=self.header)
        self.assertEquals(response.status_code, 200)
        response = json.loads(response.data)
        self.assertEquals(len(response['result']), 1)

    def test_put_maps(self):
        response = self.client.put('/api/maps/SP', headers=self.header)
        self.assertEquals(response.status_code, 200)
        response = json.loads(response.data)
        self.assertEqual(response['result']['name'], 'SP')

    def test_get_routes(self):
        response = self.client.get('/api/routes', headers=self.header)
        self.assertEquals(response.status_code, 200)
        response = json.loads(response.data)
        self.assertEquals(len(response['result']), 2)

    def test_post_routes_unauthorized(self):
        headers_to_test = [
            {'Content-Type': 'application/json'},
            {'Content-Type': 'application/json', 'Authorization': 'Login apikey="invalid key"'},
            {'Content-Type': 'application/json', 'Authorization': 'Test apikey="invalid key"'},
            {'Content-Type': 'application/json', 'Authorization': 'Test apikey=""'},
            {'Content-Type': 'application/json', 'Authorization': 'Test apikey="invalid"'},
            {'Content-Type': 'application/json', 'Authorization': 'Test apikey="%s"' % self.app.signer.sign('invalid')}
        ]
        for headers in headers_to_test:
            response = self.client.post('/api/routes', headers=headers, content_type = 'application/json')
            self.assertEquals(response.status_code, 401)

    def test_post_routes(self):
        data=json.dumps({"map": "SP", "origin": "A", "destination": "D", "autonomy": "10", "gas": "2.50"})
        response=self.client.post('/api/routes',headers=self.header,
                       data=data,
                       content_type = 'application/json')
        self.assertEquals(response.status_code, 200)
        response = json.loads(response.data)
        self.assertEqual(response['result']['price'], 6.25)
        self.assertEqual(response['result']['distance'], 25.0)
        self.assertEqual(response['result']['path'], ["A", "B", "D"])

    def test_put_routes(self):
        data=json.dumps({"map": "SP", "origin": "A", "destination": "B", "distance": "10"})
        response = self.client.put('/api/routes', headers=self.header, data=data, content_type = 'application/json')
        self.assertEquals(response.status_code, 201)
        response = json.loads(response.data)
        self.assertEquals(len(response['result']), 1)
        self.assertEqual(response['result'][0]['name'], "SP")
        self.assertEqual(response['result'][0]['origin'], "A")
        self.assertEqual(response['result'][0]['destination'], "B")

    def test_delete_routes(self):
        data=json.dumps({"map": "SP", "origin": "A", "destination": "B"})
        response=self.client.delete('/api/routes',headers=self.header,
                       data=data,
                       content_type = 'application/json')
        self.assertEquals(response.status_code, 202)

    def test_delete_maps(self):
        response=self.client.delete('/api/maps/SP',headers=self.header,
                       content_type = 'application/json')
        self.assertEquals(response.status_code, 202)
