## Solução ##
Ao analisar o problema proposto, resolvi escrever a api de forma que fosse fácil de ser executada e testada,
inicialmente pensei em inserir os mapas e rotas em um arquivo json, mais gostaria que fosse criado métodos para alimentar a base,
então descartei essa possibilidade. Cheguei a pensar em usar o mongoDB, Neo4j que usa conceito de grafos, Docker para instalação,
porém achei algo robusto demais para um simples problema, causando mais trabalho para quem fosse executar o projeto em termos de instalação.
Por esses motivos acabei usando o SQLite, pois além de ser compacto, ele pode ser diponibilizado junto com a aplicação e criei um arquivo manage.py para facilitar a execução e instalação.

O software foi desenvolvido usando o framework Flask e SQLAlchemy para camada de dados, além de ser um micro framework fácil de de trabalhar com APIs, torna mais fácil o processo de migração de bancos de dados, caso necessite.

Para a solução do problema de encontrar o menor valor de entrega e seu caminho, apliquei o conceito de grafos e grafo ponderado, onde cada arco tem um número associado a ele. Como já desenvolvi uma aplicação de logistica do zero no passado, não deu muito trabalho essa parte.

Também me preocupei um pouco com segurança criando uma chave de api, então é imprescindível que antes de executar o webservice, seja criado uma chave de api executando o método generate_api_key do manage.

Finalizando... desenvolvi a api pensando em simplificar sua instalação e execução do projeto.

> Deixei um backup das requisições do POSTMAN na pasta "docs".

## Executar aplicação ##

```
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
python manage.py runserver

```
## Executando os testes ##
```
source .venv/bin/activate
pip install -r requirements-devel.txt
nosetests -w tests/
```
## Configurando chave de API ##
```
python manage.py generate_api_key teste
```
Exemplo de saída gerada:
```
teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc
```

## Mapa ##

- `name: ` nome do mapa
- `created: ` data de criação
- `updated: ` data de atualização

###### Consultando mapas ######

> Para consultar todos mapas( previamente criados) basta fazer uma consulta get na URL.

* GET http://localhost:5000/api/maps

Response:
```
{
    "result": [
        {
            "created": "Fri, 29 Jan 2016 14:15:34 GMT",
            "name": "SP",
            "updated": "Fri, 29 Jan 2016 14:15:34 GMT"
        },
        {
            "created": "Fri, 29 Jan 2016 01:12:25 GMT",
            "name": "RJ",
            "updated": "Fri, 29 Jan 2016 01:12:25 GMT"
        }
    ]
}
```

> Para consultar um mapa( previamente criado) basta fazer uma consulta get na URL passando o nome do mapa como parâmetro.

* GET http://localhost:5000/api/maps/SP

Response:
```
{
    "result": [
        {
            "created": "Fri, 29 Jan 2016 14:15:34 GMT",
            "name": "SP",
            "updated": "Fri, 29 Jan 2016 14:15:34 GMT"
        }
    ]
}
```
###### Exemplo: ######

```
curl -X GET -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/maps
```

###### Criando mapas ######

> Para criar um mapa, basta fazer uma chamada PUT, passando o nome do mapa como parâmetro, se o nome do mapa já existir cadastrado, a api apenas retornará o registro.

* PUT http://localhost:5000/api/maps/SP
Response:
```
{
    "result": {
        "created": "Sat, 30 Jan 2016 11:47:33 GMT",
        "id": 12,
        "name": "SP",
        "updated": "Sat, 30 Jan 2016 11:47:33 GMT"
    }
}
```
###### Exemplo: ######
```
curl -X PUT -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' htp://localhost:5000/api/maps/RO
```


###### Excluindo mapas ######
> Para excluir um mapa, basta fazer uma chamada DELETE, passando o nome do mapa como parâmetro, se o nome do mapa não existir na base, é retornado 404 .
* DELETE http://localhost:5000/api/maps/SP

Response:
```
202
```
###### Exemplo: ######

curl -X DELETE -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/maps/RO

---

## Rotas ##

 - `map: ` nome do mapa
 - ` origin: ` ponto de origem de dentro do mapa
 - ` destination: ` ponto de destino de dentro do mapa
 - ` autonomy: ` consumo de combustível do automóvel por KM
 - ` gas: ` valor do combustível


###### Consultando rotas cadastradas ######

> Para consultar todas rotas( previamente cadastradas) basta fazer uma consulta GET na URL.
* GET http://localhost:5000/api/routes

Response:
```
{
    "result": [
        {
            "destination": "B",
            "distance": 10,
            "origin": "A"
        },
        {
            "destination": "D",
            "distance": 15,
            "origin": "B"
        },
        {
            "destination": "C",
            "distance": 20,
            "origin": "A"
        },
        {
            "destination": "D",
            "distance": 30,
            "origin": "C"
        },
        {
            "destination": "E",
            "distance": 50,
            "origin": "B"
        },
        {
            "destination": "E",
            "distance": 30,
            "origin": "D"
        }
    ]
}
```
###### Exemplo: ######

```
curl -X GET -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/routes
```

> Para consultar todas rotas( previamente cadastradas) de um determinado mapa, basta fazer uma consulta GET na URL passando o nome do mapa como parâmetro.
* GET http://localhost:5000/api/routes/RO

Response:
```
{
    "result": [
        {
            "destination": "B",
            "distance": 20,
            "name": "RO",
            "origin": "A"
        },
        {
            "destination": "E",
            "distance": 60,
            "name": "RO",
            "origin": "C"
        }
    ]
}
```
###### Exemplo: ######
```
curl -X GET -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' htp://localhost:5000/api/routes/RO
```

###### Criando rotas ######

> Para criar uma rota, basta fazer uma chamada PUT, passando os parâmetros json e URL abaixo. Caso a rota já estiver criada com os mesmos valores, a api apenas retornará o registro. Se o mapa não estiver cadastrado, a api criara o mapa.

* PUT http://localhost:5000/api/routes
Request:
```
{
  "map": "SP",
  "origin": "D",
  "destination": "E",
  "distance": "20"
}
```
Response:
```
{
    "result": [
        {
            "destination": "E",
            "distance": 20,
            "name": "SP",
            "origin": "D"
        }
    ]
}
```
###### Exemplo: ######

```
curl -X PUT -d '{"map": "SP","origin": "A","destination": "D","distance": "10"}'  -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/routes
```

---

###### Consultando melhor rota de entrega ######

> Para consultar rotas, menor custo e valor de entrega, basta fazer uma consulta POST, passando os parâmetros json e URL abaixo:
> Se a rota ou mapa não existir no banco de dados, a api retornará "400 Bad Request"

* POST http://localhost:5000/api/routes

Request:
```
{
    "map": "SP",
    "origin": "A",
    "destination": "D",
    "autonomy": "10",
    "gas": "2.50"
}
```

Response:
```
{
    "result": {
        "distance": 25.0,
        "path": [
            "A",
            "B",
            "D"
        ],
        "price": 6.25
    }
}
```
###### Exemplo: ######

```
curl -X POST -d '{"map": "SP","origin": "A","destination": "D","autonomy": "10","gas": "2.50"}'  -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/routes
```

###### Excluindo rotas ######
> Para excluir uma rota, basta fazer uma chamada DELETE, passando os parâmetros json e executando URL abaixo, se o nome do mapa ou rota não existir na base, é retornado 404 .
* DELETE http://localhost:5000/api/routes
Request:
```
{
  "map": "RO",
  "origin": "A",
  "destination": "B"
}
```

Response:
```
202
```

###### Exemplo: ######
curl -X DELETE -d '{"map": "RO","origin": "A","destination": "B"}'  -H "Content-Type: application/json" -H 'Authorization: apikey="teste.CYksoA.qb3MNGjzxeC9mVDbuOOznzQOywc"' http://localhost:5000/api/routes
