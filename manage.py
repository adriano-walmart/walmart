from flask import current_app
from flask.ext.script import Manager

from delivery import create_app

manager = Manager(create_app)

@manager.command
def generate_api_key(name):
    print current_app.signer.sign(name)


if __name__ == '__main__':
    manager.run()
