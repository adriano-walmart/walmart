# coding: utf-8

import datetime
from sqlalchemy import DateTime, Date
from delivery import db
from sqlalchemy.inspection import inspect
__all__ = ['BaseModel']


class BaseModel(db.Model):
    __abstract__ = True

    created = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                     nullable=False)
    updated = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                     onupdate=datetime.datetime.utcnow, nullable=False)

    def save(self, commit=True, session=None):
        if session is None:
            session = db.session

        session.add(self)
        if commit:
            session.commit()

    def delete(self, commit=True):
        session = db.session

        session.delete(self)
        if commit:
            session.commit()

    @classmethod
    def get_or_create(cls, **kwargs):
        new = False
        obj = cls.query.filter_by(**kwargs).first()
        if not obj:
            obj = cls(**kwargs)
            new = True
        return obj, new

    def __getitem__(self, key):
        return getattr(self, key)

    def keys(self):
        return inspect(self).attrs.keys()