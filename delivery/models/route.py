# -*- coding: utf-8 -*-
from sqlalchemy.orm import relationship

from delivery import db
from delivery.models.base import BaseModel


class Route(BaseModel):
    __tablename__ = 'route'
    id = db.Column(db.Integer, primary_key = True)
    origin = db.Column(db.String(255))
    destination = db.Column(db.String(255))
    distance = db.Column(db.String())
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'), nullable=False)

    def __init__(self, *args, **kargs):
        super(Route, self).__init__(*args, **kargs)