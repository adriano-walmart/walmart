# -*- coding: utf-8 -*-

from delivery import db
from base import BaseModel


class Map(BaseModel):
    __tablename__ = 'map'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50))

    def __init__(self, *args, **kargs):
        super(Map, self).__init__(*args, **kargs)

