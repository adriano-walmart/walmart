import json

from flask import jsonify, request, Response, session, Blueprint
from sqlalchemy.orm.exc import NoResultFound

from utils import requires_api_key, requires_json, as_dict
from shortest import Shortest

from delivery import db
from delivery.models import Route, Map

from werkzeug.exceptions import BadRequest, NotFound

api = Blueprint('api', __name__, url_prefix='/api')


@api.route('/maps', methods=['GET'])
@api.route('/maps/<name>', methods=['GET'])
@requires_api_key
def get_maps(name=None):
    cols = ['name', 'created', 'updated']
    query = Map.query
    if name:
        query = query.filter(Map.name == name)
    data = query.all()
    result = as_dict(data, cols)
    return jsonify(result=result)


@api.route('/maps/<name>', methods=['PUT'])
@requires_api_key
def put_maps(name):
    maps, new = Map.get_or_create(name=name)
    maps.save()
    return jsonify(result=dict(maps)), 201 if new else 200


@api.route('/maps/<name>', methods=['DELETE'])
@requires_api_key
def delete_maps(name):
    try:
        maps = Map.query.filter_by(name=name).one()
    except NoResultFound:
        raise NotFound("Map %s does not exists." % name)

    maps.delete(maps.id)
    return Response(status=202)


@api.route('/routes', methods=['GET'])
@api.route('/routes/<name>', methods=['GET'])
@requires_api_key
def get_routes(name=None):
    cols = [ 'name', 'origin', 'destination', 'distance']
    query = Route.query.join(Map)
    query = query.add_columns(Map.name, Route.origin, Route.destination, Route.distance)
    if name:
        query = query.filter(Map.name==name)

    data = query.all()
    result = as_dict(data, cols)
    return jsonify(result=result)


@api.route('/routes', methods=['PUT'])
@requires_api_key
def put_routes():
    cols = ['name', 'origin', 'destination', 'distance']
    data = request.json
    name  = data['map']
    origin  = data['origin']
    destination  = data['destination']
    distance  = float(data['distance'])
    maps, new = Map.get_or_create(name=name)
    maps.save()

    routes, new = Route.get_or_create(
        map_id = maps.id,
        origin = origin,
        destination = destination,
        distance = distance
        )
    routes.save()

    data = Route.query.join(Map).add_columns(
        Map.name,
        Route.origin,
        Route.destination,
        Route.distance
        ).filter(Route.id==routes.id).all()

    result = as_dict(data, cols)
    return jsonify(result=result), 201 if new else 200


@api.route('/routes', methods=['DELETE'])
@requires_api_key
def delete_routes():
    data = request.json
    name  = data['map']
    origin  = data['origin']
    destination  = data['destination']
    try:
        maps = Map.query.filter_by(name=name).one()
    except NoResultFound:
        raise NotFound("Map %s does not exists." % name)
    try:
        routes = Route.query.filter_by(
            map_id=maps.id,
            origin=origin,
            destination=destination).one()
    except NoResultFound:
        raise NotFound("Route %s or %s does not exists." % (origin, destination))

    routes.delete(routes.id)
    return Response(status=202)


@api.route('/routes', methods=['POST'])
@requires_json
@requires_api_key
def post_routes():
    data = request.json
    maps  = data['map']
    origin  = data['origin']
    destination  = data['destination']
    autonomy  = float(data['autonomy'])
    gas  = float(data['gas'])

    routes = (Route.query.with_entities(Route.origin, Route.destination, Route.distance)
            .join(Map)
            .filter(Map.name==maps))
    try:
        shortest = Shortest(routes)
        shortest = shortest.path(origin, destination)
        distance = float(shortest['distance'])
        path = shortest['path']
    except TypeError:
        raise BadRequest("No registered route.")

    price = (distance / autonomy) * gas
    result = {'path': path, 'distance': distance, 'price': price}
    return jsonify(result=result)

