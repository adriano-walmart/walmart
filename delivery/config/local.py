import os
from . import BaseConfiguration


class Configuration(BaseConfiguration):
    basedir = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')
    DEBUG = True
    SIGNER_KEY = 'walmart'
