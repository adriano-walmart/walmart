import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from itsdangerous import TimestampSigner


db = SQLAlchemy()

def set_base_conf(app):
    local_settings_found = os.path.exists(
        os.path.join(os.path.dirname(__file__), 'config', 'local.py'))
    env = os.environ.get(
        'WALMART_APPLICATION_SETTINGS', 'local' if local_settings_found else 'base')

    app.config.from_object('delivery.config.%s.Configuration' % env)


def create_app(set_conf=None):
    app = Flask(__name__)

    if set_conf is None:
        set_conf = set_base_conf

    set_conf(app)
    db.init_app(app)

    app.signer = TimestampSigner(app.config['SIGNER_KEY'])

    from delivery.api import api

    app.register_blueprint(api)

    return app